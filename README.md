# Description
This is a testspace for playing with workflows, in particular how to distribute large
numbers of jobs using some yadage features and implementation in REANA.

# Running
Each of the workflows can be run following the directions outlined on the [RECAST docs](https://recast-docs.web.cern.ch/recast-docs/workflowauthoring/running/).
There is test data included in the `recast.yml` files for test jobs, so after adding
a workflow to the registry 
```
$(recast catalogue add workflow_dynamic_two_piece)
```
it can be run directly
```
recast run dynamic/two_piece --backend docker --tag twopiece-0
```

# Workflows Available
There are a few workflows here with descriptions of what they do. They should sequentially
get more complex and after describing the workflow, there is a description of what would
be desired in the workflow which should be addressed in the subsequent workflows.

## workflow_dynamic_batch
This is taken from [mapreduce_batched](https://github.com/yadage/yadage/blob/master/tests/testspecs/mapreduce_batched) 
and is meant to show how job distribution can dynamically occur for a set of input parameters.  
This can be thought of analogously to how the grid distributes jobs over an input dataset container. 
There will be a parameter that eventually dictates how many jobs will have to be run to process
a given list of samples.

The important thing that this does is that for a 
`multistep-stage` step of a workflow, it allows you to pass a list parameter (in this case
a list of integers) and have
the job dynamically spawn a number of jobs where each job is provided with a number (`batchsize`)
inputs from that list.  These jobs are then gathered.

__Desired Improvement__ : It would be nice if this used a setup closer to what we want - i.e.
using (1) a docker environment (2) with an analysis and (3) an `hadd` merging of the output files.

## workflow_dynamic_two_piece
This uses a [simple analysis](https://gitlab.cern.ch/meehan/dynamicworkflows/-/blob/master/AnalysisExample/analysis.cpp) 
that takes as an argument a sequence of integers and fills these into a histogram, like this.
```
./analysis.exe 1 3 6
```
This is meant to function as a template analysis where one can imagine that each of these integers is an input file
and what normally comes out of an analysis, histograms.  This analysis is built into an image stored in the 
[registry of this repository](https://gitlab.cern.ch/meehan/dynamicworkflows/container_registry). The outputs
of each of these jobs is then gathered and merged using `hadd` into a single output root file which is then 
fed into a simple [DrawHist.py](https://gitlab.cern.ch/meehan/dynamicworkflows/-/blob/master/AnalysisExample/DrawHist.py)
script, which is like one of those analysis "post-processing" steps.

__Desired Improvement__ : It would be nice if we could store the list of input integers (which are like the input DAOD files)
in a separate text file, or yaml file, which can be fed in and parsed to the top level yaml.

## workflow_dynamic_multi_workflow
This is adapted from [stackednestings](https://github.com/yadage/yadage/blob/master/tests/testspecs/stackednestings) 
and is meant to show how workflows can be linked together and therefore "distributed".  By this,
we mean that multiple users can all write their own separate workflows and they can all be
tied together, only having to know the inputs and outputs ("parameterization") of each
individual workflow.

In this example, we run two analyses which both are currently copies of the analyses used
in `workflow_dynamic_two_piece`.  The merged output files are then picked off as parameterized
*outputs* and used as *inputs* in subsequent stages of the top level workflow which adds these 
outputs further after applying some weighting to the histograms.

__Desired Improvement__ : It would be nice if the sub workflows were distributed across 
multiple GitLab repositories.  This should require that you point at the workflow in the `$ref` 
call using something like an `https://` full URL, and not just the local paths.



## workflow_http_ref
This is similar to the `two_piece` workflow, with the slight modification what the second analysis
workflow is being referenced not from the local workflow directory, but from an `http` reference
the the remote location [https://gitlab.cern.ch/meehan/dynamicworkflows/-/tree/master/workflow_http_remote](https://gitlab.cern.ch/meehan/dynamicworkflows/-/tree/master/workflow_http_remote).

This can be very useful when there are multiple teams working at different paces on their own workflows
that will ultimately have to be tied together and flow into a single "combination", for example.

The key here is really that you need to use the `raw` link and not the `blob` link from GitLab/GitHub.

__Desired Improvement__ : The actual place where the workflow gets pulled from doesn't seem 
to be preserved in the log files.  Did I miss this?  It may be nice if in the full workflow 
diagram that gets dumped it actually prints on there some more info like this to allow for 
debugging of how things tie together.


# ToDo
This is a list of example functionality that still needs to be explored :
  - Accessing files on the grid via a grid proxy - [example from CMS](https://awesome-workshop.github.io/gitlab-cms/03-vomsproxy/index.html)

